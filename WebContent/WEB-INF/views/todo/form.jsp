<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:if test="${errors != null}">
	<div id="flush_error">
		入力内容にエラーがあります。
		<c:forEach var="error" items="${errors}">
             ・<c:out value="${error}" />
		</c:forEach>
	</div>
</c:if>

<div class="form-group">
	<label for="title">タイトル</label> 
	<input class="form-control"	type="text" id="title" name="title" value="${todo.title}" />

	<label for="place">場所</label> 
	<input class="form-control" type="text"
		id="place" name="place" value="${todo.place}" /> 
		
	<label	for="deadline">締め切り</label> 
      <div class="input-group date" id="timepicker" data-target-input="nearest">
          <input type="text" id="deadline" class="form-control datetimepicker-input" data-target="#timepicker" />
          <div class="input-group-append" data-target="#timepicker" data-toggle="datetimepicker">
              <div class="input-group-text"><i class="fa fa-calendar"></i></div>
          </div>
      </div>			

	<label for="detail">詳細</label>
	<textarea class="form-control" id=detail name="detail">${todo.content}</textarea>
	
	<br />
	<br />
	
	<button class="btn btn-primary" id="save">保存</button>

</div>
