<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:import url="/WEB-INF/views/layout/app.jsp">
    <c:param name = "content">
		<c:import url="form.jsp" />
		<p><a href = "<c:url value = '/index.html '/>">一覧に戻る</a></p>
		<button class="btn btn-danger" id="delete">削除する</button>
	</c:param>
</c:import>
<script>
	var save = document.getElementById("save");
	var deleteButton = document.getElementById("delete");
	save.onclick = function() {
		var title = document.getElementById("title").value;
		var place = document.getElementById("place").value;
		var deadline = new Date(document.getElementById("deadline").value).valueOf();
		var content = document.getElementById("detail").value;
		fetch("<c:url value ="/update" />",{
			method : "post",
			body : JSON.stringify({
				id : <c:out value="${todo.id}" />,
				title,
				place,
				deadline,
				content
			})
		}).then(function(r){
			location.replace("<c:url value ="/" />")
		})
	};
	deleteButton.onclick = function() {
		fetch("<c:url value ="/delete" />",{
			method : "post",
			body : JSON.stringify({
				id : <c:out value="${todo.id}" />,
			})
		}).then(function(r){
			location.replace("<c:url value ="/" />")
		})		
	};
    $(function () {
        $('#timepicker').datetimepicker({
        	format:"YYYY-MM-DD hh:mm:ss",
        	defaultDate: new Date("<c:out value="${todo.deadline}" />")
        });
    });
</script>