<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<style>
	#todo_form {
		padding: 50px 50px 50px 50px;
	}

</style>

<c:import url ="../layout/app.jsp" >
	<c:param name = "content">
    	<div id = "todo_form" class= "card">
			<c:import url ="./new.jsp" />
    	</div>
        <table id = "todo_list" class="table">
            <thead>
                <tr>
                    <th>タイトル</th>
                    <th>締め切り</th>
                    <th>場所</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach var = "todo" items = "${todos}" varStatus="status">
                    <tr class ="linkingRow">
                        <td><c:out value ="${todo.title}"/></td>
                        <td><c:out value = "${todo.deadline}"/></td>
                        <td><c:out value = "${todo.place}"/></td>
                        	<td class="link"><a href="<c:url value ="/edit?id=${todo.id}" />" ></a></td>
                     </tr>
                </c:forEach>
            </tbody>
        </table>
    </c:param>
</c:import>
<script>
	$(".linkingRow").click(function(){
		var c = this.children[this.children.length-1]
		c.children[0].click();
	})
</script>
