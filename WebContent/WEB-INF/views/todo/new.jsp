<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:import url="form.jsp" />
<script>
	var button = document.getElementById("save");
	button.onclick = function() {
		var title = document.getElementById("title").value;
		var place = document.getElementById("place").value;
		var deadline = new Date(document.getElementById("deadline").value).valueOf();
		var content = document.getElementById("detail").value;
		fetch("<c:url value ="/create" />",{
			method : "post",
			body : JSON.stringify({
				title,
				place,
				deadline,
				content
			})
		}).then(function(r){
			location.reload()
		})
	}
    $(function () {
        $('#deadline').datetimepicker({
        	format:"YYYY-MM-DD hh:mm:ss",
        	defaultDate: new Date()
		});
    });
</script>
