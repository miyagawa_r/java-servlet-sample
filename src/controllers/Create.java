package controllers;

import java.io.IOException;
import java.sql.Timestamp;

import javax.persistence.EntityManager;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import models.dto.TodoData;
import models.requests.CreateRequestBuilder;
import models.requests.CreateRequestBuilder.CreateRequest;
import utils.DBUtil;

/**
 * Servlet implementation class Index
 */
@WebServlet("/create")
public class Create extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Create() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		System.out.println("Create Servlet : Called");
		final CreateRequestBuilder builder = new CreateRequestBuilder();
		final CreateRequest req = (CreateRequest) builder.build(request);
		final TodoData newData = new TodoData();
		final Timestamp currentTime = new Timestamp(System.currentTimeMillis());

		
		newData.setTitle(req.getTitle());
		newData.setDeadline(req.getDeadline());
		newData.setPlace(req.getPlace());
		newData.setContent(req.getContent());
		newData.setRemark("");
		newData.setUpdated_at(currentTime);
		newData.setCreated_at(currentTime);

		EntityManager em = DBUtil.createEntityManager();
		em.getTransaction().begin();
		em.persist(newData);
		em.getTransaction().commit();
		em.close();

		response.setStatus(200);
	
	}

}
