package controllers;

import java.io.IOException;
import java.sql.Timestamp;

import javax.persistence.EntityManager;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import models.dto.TodoData;
import models.requests.DestroyRequestBuilder;
import models.requests.DestroyRequestBuilder.DestroyRequest;
import utils.DBUtil;

/**
 * Servlet implementation class Index
 */
@WebServlet("/delete")
public class Delete extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Delete() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		final DestroyRequestBuilder builder = new DestroyRequestBuilder();
		final DestroyRequest req = (DestroyRequest) builder.build(request);
		final Timestamp currentTime = new Timestamp(System.currentTimeMillis());
		final EntityManager em = DBUtil.createEntityManager();
		final TodoData originalData = em.find(TodoData.class, req.getId());
		final String contextPath = request.getContextPath();

		if(originalData == null){
			response.setStatus(500);
			response.getWriter().write("cannot find any todo datas.");
			return;
		}
		
		originalData.setDeleted_at(currentTime);
		em.getTransaction().begin();
		em.getTransaction().commit();
		em.close();

		response.sendRedirect(contextPath+"/");
		return ;

	}

}
