package controllers;

import java.io.IOException;

import javax.persistence.EntityManager;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import models.dto.TodoData;
import utils.DBUtil;

/**
 * Servlet implementation class Index
 */
@WebServlet("/edit")
public class Edit extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Edit() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		int todoID = Integer.parseInt(request.getParameter("id"));
        EntityManager em = DBUtil.createEntityManager();

        TodoData todo = em.find(TodoData.class, todoID);
		 System.out.println(todo.getDeadline());
        request.setAttribute("todo",todo);
        RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/views/todo/edit.jsp");
        rd.forward(request, response);
	}

}
