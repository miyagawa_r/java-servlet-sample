package models.requests;

import java.sql.Timestamp;
import java.util.Map;


public class UpdateRequestBuilder extends RequestBuilder{
	
	public class UpdateRequest {
		public UpdateRequest(
				int id,
				Timestamp deadline,
				String title,
				String content,
				String place
			) {
			this.id = id;
			this.deadline = deadline;
			this.title = title;
			this.content = content;
			this.place = place;
			
		}
		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		public Timestamp getDeadline() {
			return deadline;
		}
		public void setDeadline(Timestamp deadline) {
			this.deadline = deadline;
		}
		public String getTitle() {
			return title;
		}
		public void setTitle(String title) {
			this.title = title;
		}
		public String getContent() {
			return content;
		}
		public void setContent(String content) {
			this.content = content;
		}
		public String getPlace() {
			return place;
		}
		public void setPlace(String place) {
			this.place = place;
		}
		private int id;
		private Timestamp deadline;
		private String title;
		private String content;
		private String place;
	}
    
	@SuppressWarnings("unchecked")
	public Object fromRequest(Object req) {
		final Map<String, Object> json = (Map<String, Object>)req;
    	return (Object) (new UpdateRequest(
    		Integer.parseInt((String)json.get("id")),
			new Timestamp(Long.parseLong((String)json.get("deadline"))),
			(String)json.get("title"),
			(String)json.get("content"),
			(String)json.get("place")
		));
    }
    
}
