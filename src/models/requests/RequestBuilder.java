package models.requests;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

public abstract class RequestBuilder {
	public Object build(HttpServletRequest req) throws IOException {
		BufferedReader buffer = new BufferedReader(req.getReader());
		String reqJson = buffer.readLine();
		ObjectMapper mapper = new ObjectMapper();
		Map<String, String> reqMap = 
		mapper.readValue(reqJson, new TypeReference<Map<String, String>>() {});		
		return this.fromRequest(reqMap);
	}
	
	public abstract Object fromRequest(Object arg);
}
