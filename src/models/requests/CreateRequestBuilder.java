package models.requests;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Map;


public class CreateRequestBuilder extends RequestBuilder{
	
	public class CreateRequest {
		public CreateRequest(
				Timestamp deadline,
				String title,
				String content,
				String place
			) {
			this.deadline = deadline;
			this.title = title;
			this.content = content;
			this.place = place;
			
		}
		public Timestamp getDeadline() {
			return deadline;
		}
		public void setDeadline(Timestamp deadline) {
			this.deadline = deadline;
		}
		public String getTitle() {
			return title;
		}
		public void setTitle(String title) {
			this.title = title;
		}
		public String getContent() {
			return content;
		}
		public void setContent(String content) {
			this.content = content;
		}
		public String getPlace() {
			return place;
		}
		public void setPlace(String place) {
			this.place = place;
		}
		private Timestamp deadline;
		private String title;
		private String content;
		private String place;
	}
    
	@SuppressWarnings("unchecked")
	public Object fromRequest(Object req) {
		final Map<String, Object> json = (Map<String, Object>)req;
    	return (Object) (new CreateRequest(
			new Timestamp(Long.parseLong((String)json.get("deadline"))),
			(String)json.get("title"),
			(String)json.get("content"),
			(String)json.get("place")
		));
    }
    
}
