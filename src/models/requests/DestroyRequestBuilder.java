package models.requests;

import java.util.Map;


public class DestroyRequestBuilder extends RequestBuilder{
	
	public class DestroyRequest {
		public DestroyRequest(int id) {
			this.id = id;
		}
		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}

		private int id;
	}
    
	@SuppressWarnings("unchecked")
	public Object fromRequest(Object req) {
		final Map<String, Object> json = (Map<String, Object>)req;
    	return (Object) (new DestroyRequest(
    		Integer.parseInt((String)json.get("id"))
		));
    }
    
}
